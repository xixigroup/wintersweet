
from starlette.exceptions import HTTPException
from fastapi.middleware.cors import CORSMiddleware

from views import router
from wintersweet.framework.fastapi.exception_handlers import http_exception_handler, runtime_exception_handler
from wintersweet.utils.logging import DEFAULT_TRACE_LOG_FORMAT
from wintersweet.framework.fastapi.middlewares import RequestIDMiddleware

DEBUG = False
SECRET = f'__access_control_{DEBUG}_@5806fecc-93b3-11ec-beda-e2b55ff3da64__'

LISTEN_PORT = 8080

# process
PROCESS_NUM = 2


APP_CONFIG = {
    'debug': DEBUG,
    'routes': router.routes,
}


LOGGING_CONFIG = {
    'level': 'debug',
    'file_path': '',
    'file_rotation': None,
    'file_retention': None,
    # 'log_format': DEFAULT_TRACE_LOG_FORMAT
}
from starlette.middleware.errors import ServerErrorMiddleware

# MIDDLEWARES
MIDDLEWARES = [
    {
        'cls': RequestIDMiddleware,
        'options': {}
    },
    {
        'cls': ServerErrorMiddleware,
        'options': {
            'handler': runtime_exception_handler
        }

    },
    {
        'cls': CORSMiddleware,
        'options': {
            'allow_origins': '*',
            'allow_methods': '*',
            'allow_headers': '*',
            'allow_credentials': True
        }
    },

]

EXCEPTION_HANDLERS = [
    {
        'cls': HTTPException,
        'handler': http_exception_handler
    },
]

DATABASES = {
    # 'default': {
    #     'echo': True,
    #     'host': "localhost",
    #     'user': 'root',
    #     'password': "",
    #     'db': 'test',
    #     'port': 3306,
    #     'charset': r'utf8',
    #     'autocommit': True,
    #     'cursorclass': aiomysql.DictCursor,
    # }
}

REDIS_CONFIG = {

}

INTERVAL_TASKS = [
    # (3, func),
]

NTP_CONFIG = {
    'interval': 3600,
    'version': 2,
    'port': 'ntp',
    'timeout': 5,
    'hosts': ['ntp2.aliyun.com', 'ntp3.aliyun.com', 'ntp4.aliyun.com', 'ntp5.aliyun.com']
}