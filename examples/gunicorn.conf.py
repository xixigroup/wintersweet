

# -*- coding: utf-8 -*-
import os

os.environ.setdefault('WINTERSWEET_SETTINGS_MODULE', 'setting')

from wintersweet.framework.conf import settings


# 进程数
workers = settings.PROCESS_NUM

# 工人类
worker_class = 'uvicorn.workers.UvicornWorker'

# 日志配置
logconfig_dict = {
    'root': {
        'level': 'INFO',
        'handlers': ['loguru'],
    },
    'loggers': {
        'gunicorn.error': {
            'level': 'INFO',
            'handlers': ['loguru'],
            'qualname': 'gunicorn.error',
        },
        'gunicorn.access': {
            'level': 'INFO',
            'handlers': ['loguru'],
            'qualname': 'gunicorn.access',
        },
    },
    'handlers': {
        'loguru': {
            'class': 'wintersweet.utils.logging.LogInterceptor',
        },
    },
}

