
from starlette.requests import Request


def _get_header_tag(key):

    def get_header(request):

        val = request.headers.get(key, None)

        return val

    return get_header


def get_remote_addr(request: Request) -> str:

    return request.client.host or "127.0.0.1"


get_authentication = _get_header_tag('Authentication')
get_ipaddr = _get_header_tag('X_FORWARDED_FOR') or "127.0.0.1"

