

class RunnableInterface:
    """Runnable抽象接口
    """

    def run(self):
        raise NotImplementedError()