
class TaskInterface:
    """
    Task抽象接口
    """

    def __init__(self):
        self._running = False

    @property
    def running(self):
        return self._running

    def start(self):
        raise InterruptedError()

    def stop(self):
        raise InterruptedError()


class AsyncTaskInterface:
    """
    异步Task抽象接口
    """

    def __init__(self):
        self._running = False

    @property
    def running(self):
        return self._running

    async def start(self):
        raise InterruptedError()

    async def stop(self):
        raise InterruptedError()

